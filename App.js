import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import {
  Provider as PaperProvider,
  TextInput,
  Headline,
  Button,
  Surface,
} from "react-native-paper";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { resultado: "", valor1: "", valor2: "" };
  }

  render() {
    return (
      <PaperProvider>
        <View style={styles.container}>
          <Headline style={styles.headline}> Bienvenido!</Headline>

          <View style={{ width: 20, height: 20 }} />
          <TextInput
            value={this.state.valor1}
            style={styles.textInput}
            label="Ingrese su usuario"
            onChangeText={this.setValor1}
          ></TextInput>

          <TextInput
            value={this.state.valor2}
            style={styles.textInput}
            secureTextEntry
            label="Ingrese contraseña"
            onChangeText={this.setValor2}
          ></TextInput>      

          {/* <StatusBar style="auto" /> */}
          <View style={{ marginBottom: 5 }}>
            <Button
              mode="contained"
              onPress={() => this.loguearse(this.state.valor1, this.state.valor2)}
            >
              Registrar
            </Button>
          </View>

          <View style={{ marginBottom: 5 }}>
            <Button
              mode="contained"
              onPress={() => this.loguearse(this.state.valor1, this.state.valor2)}
            >
              Ingresar
            </Button>
          </View>
        </View>
      </PaperProvider>
    );
  }

  setValor1 = (text) => {
    this.setState({ valor1: text });
  };

  setValor2 = (text) => {
    this.setState({ valor2: text });
  };

  loguearse(a, b) {
    debugger;
    var a1 = parseInt(a);
    var b1 = parseInt(b);

    this.setState({ resultado: a1 + b1 });
  }
 
}

const styles = StyleSheet.create({
  container: {
    //   flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    margin: "1em",
  },
  textInput: {
    margin: "1em",
  },
  surface: {
    marginTop: 10,
    padding: 8,
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center",
    elevation: 4,
    borderRadius: 16,
  },
});
